#!/usr/bin/env bash

mvn clean package

echo 'Copy files...'

scp -i ~/.ssh/springmvc.pem \
    target/springmvc-1.0-SNAPSHOT.jar \
    ec2-user@ec2-54-93-230-237.eu-central-1.compute.amazonaws.com:/home/ec2-user/

echo 'Restart server...'

ssh -tt -i ~/.ssh/springmvc.pem ec2-user@ec2-54-93-230-237.eu-central-1.compute.amazonaws.com << EOF
pgrep java | xargs kill -9
nohup java -jar springmvc-1.0-SNAPSHOT.jar > log.txt &
EOF

echo 'Bye'